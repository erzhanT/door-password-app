const initialState = {
    pass: '',
    pinCode: '1337',
    accessGranted: false,
    disabled: false
};


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ACCESS':
            if (state.pinCode === state.pass) {
                return {...state, accessGranted: true}
            }
            return state;
        case 'ADD' :
            return {...state, pass: state.pass += action.num}
        case 'DISABLE' :
            if (state.pass.length === 4) {
                return {...state, disabled: true}
            }
            return state;
        case 'REDUCE':
            return {...state, pass: state.pass.slice(0, -1)}
        default:
            return state
    }

};

export default reducer;