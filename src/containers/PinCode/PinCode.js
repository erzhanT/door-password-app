import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import './PinCode.css';


const PinCode = () => {
    const buttons = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
    const dispatch = useDispatch();
    const state = useSelector(state => state);


    const onClickHandler = (e) => {
        dispatch({type: 'ADD', num: e.target.value})
        dispatch({type: 'DISABLE'})
    }

    const checkPass = () => {
        dispatch({type: 'ACCESS'})
    }

    const clear = () => {
        dispatch({type: 'REDUCE'})
    }



    return (
        <div className="PinCode">
            <div className={state.accessGranted === true ? 'success' :
                state.pass.length === 4 &&
                state.accessGranted === false ? 'error' : 'enter'}>
                <h3 className='password'>{state.pass.length > 0 ? '*' : state.pass.length}</h3>
            </div>
            <div className="buttons">
                {buttons.map((num, index) => {
                    return (
                        <button
                            onClick={(e) => onClickHandler(e)}
                            disabled={state.disabled}
                            key={index}
                            value={num}
                        >
                            {num}
                        </button>
                    )
                })}
                <button onClick={clear}>-</button>
                <button
                    onClick={(e) => onClickHandler(e)}
                    value={'0'}
                    disabled={state.disabled}
                >
                    0
                </button>
                <button onClick={checkPass}>E</button>
            </div>
        </div>
    );
};

export default PinCode;