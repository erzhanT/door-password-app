import React from "react";
import PinCode from "./containers/PinCode/PinCode";

const App = () => <PinCode />;

export default App;
